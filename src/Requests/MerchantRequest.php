<?php

namespace Placeto\Sdk3dsServer\Requests;

use Placeto\Sdk3dsServer\Contracts\Entity;

class MerchantRequest extends Entity
{
    protected ?string $name = null;
    protected ?string $brand = null;
    protected ?string $country = null;
    protected ?string $currency = null;
    protected ?array $document = null;
    protected ?string $url = null;
    protected ?int $mcc = null;
    protected ?int $isicClass = null;
    protected ?array $branch = null;
    protected ?array $subscriptions = null;
    protected ?array $invitations = null;

    public function __construct($data)
    {
        $this->load($data, ['name', 'brand', 'country', 'currency', 'document', 'url', 'mcc', 'isicClass', 'branch',
            'subscriptions', 'invitations', ]);
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'name' => $this->getName(),
            'brand' => $this->getBrand(),
            'country' => $this->getCountry(),
            'currency' => $this->getCurrency(),
            'document' => $this->getDocument(),
            'url' => $this->getUrl(),
            'mcc' => $this->getMcc(),
            'isicClass' => $this->getIsicClass(),
            'branch' => $this->getBranch(),
            'subscriptions' => $this->getSubscriptions(),
            'invitations' => $this->getInvitations(),
        ]);
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function getDocument(): ?array
    {
        return $this->document;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getMcc(): ?int
    {
        return $this->mcc;
    }

    public function getIsicClass(): ?int
    {
        return $this->isicClass;
    }

    public function getBranch(): ?array
    {
        return $this->branch;
    }

    public function getSubscriptions(): ?array
    {
        return $this->subscriptions;
    }

    public function getInvitations(): ?array
    {
        return $this->invitations;
    }
}
