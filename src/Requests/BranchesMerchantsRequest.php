<?php

namespace Placeto\Sdk3dsServer\Requests;

use Placeto\Sdk3dsServer\Contracts\Entity;

class BranchesMerchantsRequest extends Entity
{
    protected ?array $branches = null;

    public function __construct($data)
    {
        $this->load($data, ['branches']);
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'branches' => $this->getBranches(),
        ]);
    }

    public function getBranches(): ?array
    {
        return $this->branches;
    }
}
