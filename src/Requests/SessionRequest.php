<?php

namespace Placeto\Sdk3dsServer\Requests;

use Placeto\Sdk3dsServer\Contracts\Entity;

class SessionRequest extends Entity
{
    protected ?string $acctNumber = null;
    protected ?string $cardExpiryDate = null;
    protected ?string $purchaseAmount = null;
    protected ?string $redirectURI = null;
    protected ?string $purchaseCurrency = null;

    protected ?string $threeDSAuthenticationInd = null;
    protected ?string $reference = null;
    protected ?string $recurringFrequency = null;
    protected ?string $recurringExpiry = null;
    protected ?string $purchaseInstalData = null;
    protected ?array $threeDSAuthenticationInfo = null;
    protected ?string $threeDSChallengeInd = null;
    protected ?string $threeDSRequestorID = null;
    protected ?array $acctInfo = null;
    protected ?string $acctID = null;
    protected ?string $billAddrCity = null;
    protected ?string $billAddrCountry = null;
    protected ?string $billAddrLine1 = null;
    protected ?string $billAddrLine2 = null;
    protected ?string $billAddrLine3 = null;
    protected ?string $billAddrPostCode = null;
    protected ?string $billAddrState = null;
    protected ?string $email = null;
    protected ?array $homePhone = null;
    protected ?array $mobilePhone = null;
    protected ?string $cardholderName = null;
    protected ?string $shipAddrCity = null;
    protected ?string $shipAddrCountry = null;
    protected ?string $shipAddrLine1 = null;
    protected ?string $shipAddrLine2 = null;
    protected ?string $shipAddrLine3 = null;
    protected ?string $shipAddrPostCode = null;
    protected ?string $shipAddrState = null;
    protected ?array $workPhone = null;
    protected ?array $merchantRiskIndicator = null;

    public function __construct($data)
    {
        $this->load($data, ['acctNumber', 'cardExpiryDate', 'purchaseAmount', 'purchaseCurrency', 'redirectURI',
            'threeDSAuthenticationInd', 'reference', 'recurringFrequency', 'recurringExpiry', 'purchaseInstalData',
            'threeDSAuthenticationInfo', 'threeDSChallengeInd', 'threeDSRequestorID', 'acctInfo', 'acctID',
            'billAddrCity', 'billAddrCountry', 'billAddrLine1', 'billAddrLine2', 'billAddrLine3', 'billAddrPostCode',
            'billAddrState', 'email', 'homePhone', 'mobilePhone', 'cardholderName', 'shipAddrCity', 'shipAddrCountry',
            'shipAddrLine1', 'shipAddrLine2', 'shipAddrLine3', 'shipAddrPostCode', 'shipAddrState', 'workPhone',
            'merchantRiskIndicator', ]);
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'acctNumber' => $this->getAcctNumber(),
            'cardExpiryDate' => $this->getCardExpiryDate(),
            'purchaseAmount' => $this->getPurchaseAmount(),
            'purchaseCurrency' => $this->getPurchaseCurrency(),
            'redirectURI' => $this->getRedirectURI(),
            'threeDSAuthenticationInd' => $this->getThreeDSAuthenticationInd(),
            'reference' => $this->getReference(),
            'recurringFrequency' => $this->getRecurringFrequency(),
            'recurringExpiry' => $this->getRecurringExpiry(),
            'purchaseInstalData' => $this->getPurchaseInstalData(),
            'threeDSAuthenticationInfo' => $this->getThreeDSAuthenticationInfo(),
            'threeDSChallengeInd' => $this->getThreeDSChallengeInd(),
            'threeDSRequestorID' => $this->getThreeDSRequestorID(),
            'acctInfo' => $this->getAcctInfo(),
            'acctID' => $this->getAcctID(),
            'billAddrCity' => $this->getBillAddrCity(),
            'billAddrCountry' => $this->getBillAddrCountry(),
            'billAddrLine1'=> $this->getBillAddrLine1(),
            'billAddrLine2' => $this->getBillAddrLine2(),
            'billAddrLine3' => $this->getBillAddrLine3(),
            'billAddrPostCode' => $this->getBillAddrPostCode(),
            'billAddrState' => $this->getBillAddrState(),
            'email' => $this->getEmail(),
            'homePhone' => $this->getHomePhone(),
            'mobilePhone' => $this->getMobilePhone(),
            'cardholderName' => $this->getCardholderName(),
            'shipAddrCity' => $this->getShipAddrCity(),
            'shipAddrCountry' => $this->getShipAddrCountry(),
            'shipAddrLine1' => $this->getShipAddrLine1(),
            'shipAddrLine2' => $this->getShipAddrLine2(),
            'shipAddrLine3' => $this->getShipAddrLine3(),
            'shipAddrPostCode' => $this->getShipAddrPostCode(),
            'shipAddrState' => $this->getShipAddrState(),
            'workPhone' => $this->getWorkPhone(),
            'merchantRiskIndicator' => $this->getMerchantRiskIndicator(),
        ]);
    }

    public function getAcctNumber(): ?string
    {
        return $this->acctNumber;
    }

    public function getCardExpiryDate(): ?string
    {
        return $this->cardExpiryDate;
    }

    public function getPurchaseAmount(): ?string
    {
        return $this->purchaseAmount;
    }

    public function getRedirectURI(): ?string
    {
        return $this->redirectURI;
    }

    public function getPurchaseCurrency(): ?string
    {
        return $this->purchaseCurrency;
    }

    public function getThreeDSAuthenticationInd(): ?string
    {
        return $this->threeDSAuthenticationInd;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function getRecurringFrequency(): ?string
    {
        return $this->recurringFrequency;
    }

    public function getRecurringExpiry(): ?string
    {
        return $this->recurringExpiry;
    }

    public function getPurchaseInstalData(): ?string
    {
        return $this->purchaseInstalData;
    }

    public function getThreeDSAuthenticationInfo(): ?array
    {
        return $this->threeDSAuthenticationInfo;
    }

    public function getThreeDSChallengeInd(): ?string
    {
        return $this->threeDSChallengeInd;
    }

    public function getThreeDSRequestorID(): ?string
    {
        return $this->threeDSRequestorID;
    }

    public function getAcctInfo(): ?array
    {
        return $this->acctInfo;
    }

    public function getAcctID(): ?string
    {
        return $this->acctID;
    }

    public function getBillAddrCity(): ?string
    {
        return $this->billAddrCity;
    }

    public function getBillAddrCountry(): ?string
    {
        return $this->billAddrCountry;
    }

    public function getBillAddrLine1(): ?string
    {
        return $this->billAddrLine1;
    }

    public function getBillAddrLine2(): ?string
    {
        return $this->billAddrLine2;
    }

    public function getBillAddrLine3(): ?string
    {
        return $this->billAddrLine3;
    }

    public function getBillAddrPostCode(): ?string
    {
        return $this->billAddrPostCode;
    }

    public function getBillAddrState(): ?string
    {
        return $this->billAddrState;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getHomePhone(): ?array
    {
        return $this->homePhone;
    }

    public function getMobilePhone(): ?array
    {
        return $this->mobilePhone;
    }

    public function getCardholderName(): ?string
    {
        return $this->cardholderName;
    }

    public function getShipAddrCity(): ?string
    {
        return $this->shipAddrCity;
    }

    public function getShipAddrCountry(): ?string
    {
        return $this->shipAddrCountry;
    }

    public function getShipAddrLine1(): ?string
    {
        return $this->shipAddrLine1;
    }

    public function getShipAddrLine2(): ?string
    {
        return $this->shipAddrLine2;
    }

    public function getShipAddrLine3(): ?string
    {
        return $this->shipAddrLine3;
    }

    public function getShipAddrPostCode(): ?string
    {
        return $this->shipAddrPostCode;
    }

    public function getShipAddrState(): ?string
    {
        return $this->shipAddrState;
    }

    public function getWorkPhone(): ?array
    {
        return $this->workPhone;
    }

    public function getMerchantRiskIndicator(): ?array
    {
        return $this->merchantRiskIndicator;
    }
}
