<?php

namespace Placeto\Sdk3dsServer;

use Placeto\Sdk3dsServer\Exceptions\Server3DSException;
use Placeto\Sdk3dsServer\Helpers\Settings;
use Placeto\Sdk3dsServer\Requests\BranchesMerchantsRequest;
use Placeto\Sdk3dsServer\Requests\MerchantRequest;
use Placeto\Sdk3dsServer\Requests\SessionRequest;
use Placeto\Sdk3dsServer\Responses\Response;

class Server3DS
{
    private const MESSAGE = 'Wrong class request';

    protected Settings $settings;

    public function __construct(array $data)
    {
        $this->settings = new Settings($data);
    }

    public function createMerchant($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $createMerchantRequest = new MerchantRequest($dataRequest);
        }

        if (!($createMerchantRequest instanceof MerchantRequest)) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getServer3DSOperations()->createMerchant($createMerchantRequest);
    }

    public function createSession($dataRequest): Response
    {
        if (is_array($dataRequest)) {
            $createSessionRequest = new SessionRequest($dataRequest);
        }

        if (!($createSessionRequest instanceof SessionRequest)) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getServer3DSOperations()->createSession($createSessionRequest);
    }

    public function transactionInformation($transactionID): Response
    {
        return $this->settings->getServer3DSOperations()->transactionInformation($transactionID);
    }

    public function addBranchesMerchants($dataRequest, string $merchantID): Response
    {
        if (is_array($dataRequest)) {
            $addBranchesMerchantsRequest = new BranchesMerchantsRequest($dataRequest);
        }

        if (!($addBranchesMerchantsRequest instanceof BranchesMerchantsRequest)) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getServer3DSOperations()->addBranchesMerchants($addBranchesMerchantsRequest, $merchantID);
    }
}
