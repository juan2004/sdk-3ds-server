<?php

namespace Placeto\Sdk3dsServer\Responses;

use Placeto\Sdk3dsServer\Contracts\Entity;
use Placeto\Sdk3dsServer\Exceptions\Server3DSException;

class Response extends Entity
{
    private const MESSAGE_TOKEN = 'The response is failed and there is no token in the returned response';
    protected array $response = [];

    public function __construct($data)
    {
        $this->response = $data;
    }

    public function getResponse(): array
    {
        return $this->response;
    }

    public function getToken(): string
    {
        if (empty($this->response['data']['token'])) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE_TOKEN);
        }

        return $this->response['data']['token'];
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'response' => $this->response,
        ]);
    }
}
