<?php

namespace Placeto\Sdk3dsServer\Exceptions;

use Throwable;

class Server3DSServiceException extends Server3DSException
{
    private const MESSAGE = 'Error handling operation';

    public static function fromServiceException(Throwable $e): self
    {
        return new self(self::MESSAGE, 100, $e);
    }
}
