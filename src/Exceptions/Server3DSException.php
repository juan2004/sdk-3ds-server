<?php

namespace Placeto\Sdk3dsServer\Exceptions;

use Exception;

class Server3DSException extends Exception
{
    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
