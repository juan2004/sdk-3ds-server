<?php

namespace Placeto\Sdk3dsServer\Helpers;

use GuzzleHttp\Client;
use Placeto\Sdk3dsServer\Carrier\Request;
use Placeto\Sdk3dsServer\Contracts\Entity;
use Placeto\Sdk3dsServer\Contracts\Server3DSOperations;
use Placeto\Sdk3dsServer\Exceptions\Server3DSException;

class Settings extends Entity
{
    private const MESSAGE1 = 'No token provided';
    private const MESSAGE2 = 'No service URL provided to use';

    protected string $token;
    public string $baseUrl = '';
    protected array $headers = [];

    protected int $timeout = 15;
    protected bool $verifySsl = true;

    protected ?Client $client = null;
    protected ?Server3DSOperations $server3DSOperations = null;

    public function __construct(array $data)
    {
        if (!isset($data['token'])) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE1);
        }

        if (!isset($data['baseUrl']) || !filter_var($data['baseUrl'], FILTER_VALIDATE_URL)) {
            throw Server3DSException::forDataNotProvided(self::MESSAGE2);
        }

        if (substr($data['baseUrl'], -1) != '/') {
            $data['baseUrl'] .= '/';
        }

        $allowedKeys = [
            'token',
            'baseUrl',
            'headers',
        ];

        $this->load($data, $allowedKeys);
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function baseUrl(string $endpoint = ''): string
    {
        return $this->baseUrl . $endpoint;
    }

    public function getHeaders(): array
    {
        $this->headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getToken(),
            'Content-Type' => 'application/json',
        ];

        return $this->headers;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function isVerifySsl(): bool
    {
        return $this->verifySsl;
    }

    public function getClient(): Client
    {
        if (!$this->client) {
            $this->client = new Client([
                'timeout' => $this->getTimeout(),
                'connect_timeout' => $this->getTimeout(),
                'verify' => $this->isVerifySsl(),
            ]);
        }

        return $this->client;
    }

    public function getServer3DSOperations(): Server3DSOperations
    {
        if ($this->server3DSOperations instanceof Server3DSOperations) {
            return $this->server3DSOperations;
        } else {
            $this->server3DSOperations = new Request($this);
        }

        return $this->server3DSOperations;
    }

    public function toArray(): array
    {
        return [];
    }
}
