<?php

namespace Placeto\Sdk3dsServer\Contracts;

use Placeto\Sdk3dsServer\Helpers\ArrayHelper;
use Placeto\Sdk3dsServer\Traits\LoaderTrait;

abstract class Entity
{
    use LoaderTrait;

    abstract public function toArray(): array;

    protected function arrayFilter(array $array): array
    {
        return ArrayHelper::filter($array);
    }
}
