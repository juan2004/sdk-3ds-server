<?php

namespace Placeto\Sdk3dsServer\Contracts;

use Placeto\Sdk3dsServer\Helpers\Settings;
use Placeto\Sdk3dsServer\Requests\BranchesMerchantsRequest;
use Placeto\Sdk3dsServer\Requests\MerchantRequest;
use Placeto\Sdk3dsServer\Requests\SessionRequest;
use Placeto\Sdk3dsServer\Responses\Response;

abstract class Server3DSOperations
{
    protected Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    abstract public function createMerchant(MerchantRequest $createMerchantRequest): Response;

    abstract public function createSession(SessionRequest $createSessionRequest): Response;

    abstract public function transactionInformation(string $transactionID): Response;

    abstract public function addBranchesMerchants(BranchesMerchantsRequest $branchesMerchantsRequest, string $merchantID): Response;
}
