<?php

namespace Placeto\Sdk3dsServer\Carrier;

use GuzzleHttp\Exception\BadResponseException;
use Placeto\Sdk3dsServer\Contracts\Server3DSOperations;
use Placeto\Sdk3dsServer\Exceptions\Server3DSServiceException;
use Placeto\Sdk3dsServer\Helpers\ArrayHelper;
use Placeto\Sdk3dsServer\Requests\BranchesMerchantsRequest;
use Placeto\Sdk3dsServer\Requests\MerchantRequest;
use Placeto\Sdk3dsServer\Requests\SessionRequest;
use Placeto\Sdk3dsServer\Responses\Response;
use Throwable;

class Request extends Server3DSOperations
{
    private const ENDPOINT_CREATE_MERCHANT = 'v1/merchants';
    private const ENDPOINT_CREATE_SESSION = 'threeds/v2/sessions';
    private const ENDPOINT_TRANSACTION_INFORMATION = 'v2/transactions';
    private const ENDPOINT_ADD_BRANCHES_MERCHANTS_1 = 'v1/merchants';
    private const ENDPOINT_ADD_BRANCHES_MERCHANTS_2 = 'branches';

    private function makeRequest(string $url, string $action, array $arguments = [])
    {
        try {
            $data = ArrayHelper::filter($arguments);

            $response = $this->settings->getClient()->$action($url, [
                'json' => $data,
                'headers' => $this->settings->getHeaders(),
            ]);

            $result = $response->getBody()->getContents();
        } catch (BadResponseException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        } catch (Throwable $e) {
            throw Server3DSServiceException::fromServiceException($e);
        }

        return json_decode($result, true);
    }

    public function createMerchant(MerchantRequest $createMerchantRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_CREATE_MERCHANT), 'post', $createMerchantRequest->toArray());

        return new Response($result);
    }

    public function createSession(SessionRequest $createSessionRequest): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_CREATE_SESSION), 'post', $createSessionRequest->toArray());

        return new Response($result);
    }

    public function transactionInformation(string $transactionID): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_TRANSACTION_INFORMATION . '/' . $transactionID), 'get');

        return new Response($result);
    }

    public function addBranchesMerchants(BranchesMerchantsRequest $branchesMerchantsRequest, string $merchantID): Response
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_ADD_BRANCHES_MERCHANTS_1 . '/' . $merchantID
            . '/' . self::ENDPOINT_ADD_BRANCHES_MERCHANTS_2), 'post', $branchesMerchantsRequest->toArray());

        return new Response($result);
    }
}
