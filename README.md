# 3DS Server SDK

Software development kit to connect with the 3DS Server's API

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placeto/sdk-3ds-server
```

The first thing you need to do is create an instance of the Server3DS class
and pass the credentials (token, baseUrl) as a parameter. Example:

```
return new Server3DS([
    'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIQvhzVU',
    'baseUrl' => 'https://3dss-test.placetopay.com/api',
]);
```

## Available methods

### createMerchant():
To create a merchant, you must call the createMerchant() method from the Server3DS class instance and pass the request
fields (array) to the API as a parameter. Example:

```
$request = [
    "name" => "EGM Ingenieria sin froteras" . mt_rand(),
    "brand" => "placetopay",
    "country" => "COL",
    "currency" => "COP",
    "document" => [
         "type" => "RUT",
         "number" => "123456789-0"
    ],
    "url" => "https://www.placetopay.com",
    "mcc" => 742,
    "isicClass" => 111,
    "branch" => [
         "name" => "Ofic principal." . mt_rand(),
         "brand" => "placetopay dos",
         "country" => "COL",
         "currency" => "COP"
    ],
    "subscriptions" => [
         [
              "franchise" => 1,
              "acquirerBIN" => 400900,
              "version" => 2
         ],
    ],
    "invitations" => [
         "admin@admin.com" => "juan.pabon@evertecinc.com"
    ]
];

$response = $this->server3DS()->createMerchant($request);
```

### createSession():
To create a new session, you must call the createSession() method from the Server3DS class instance and pass the request
fields (array) to the API as a parameter. Example:

```
$request = [
    'acctNumber' => '4111111111111111',
    'cardExpiryDate' => '2411',
    'purchaseAmount' => '8.25',
    'redirectURI' => 'http://www.placetopay.com',
    'purchaseCurrency' => 'COP'
];

$response = $this->server3DS()->createSession($request);
```

### transactionInformation():
To get transaction information, you must call the transactionInformation() method from the Server3DS class instance and 
pass the transactionID (string) to the API as a parameter. Example:

```
$transactionID = '135';

$response = $this->server3DS()->transactionInformation($transactionID);
```

### addBranchesMerchants():
To add branches to a merchant, you must call the addBranchesMerchants() method from the Server3DS class instance and 
pass the request fields (array) and the merchantID of the URL to the API as a parameter. Example:

```
$request = [
    "branches" => [
         [
              "name" => 'namebranch,
              "brand" => "Evertec Medellin",
              "country" => "COL",
              "currency" => "COP",
              "url" => "https://example-uno.com"
         ],
         [
              "name" => 'namebranch2,
              "brand" => "Evertec Bogotá",
              "country" => "COL",
              "currency" => "COP",
              "url" => "https://example-dos.com"
         ],
    ],
];

$merchantID = '135';

$response = $this->server3DS()->addBranchesMerchants($request, $merchantID);
```

#### Get only the message from the response with the getResponse() method.

```
$response->getResponse()
```

To get the token obtained in the response when creating a merchant, call the getToken() method.

```
$response->getToken()
```

#### For more information about the 3DS Server API see the documentation [Here](https://docs-3dss.placetopay.com/docs/3dss-api-docs/ZG9jOjQ2NDA2-introduccion)
