<?php

namespace Tests\Unit;

class Config
{
    public static function token(): string
    {
        return 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMzliZGQxOWZlMGMyNmM1NzdjMjgzMDNlNTQ0OTU5MDk0NWVkMmE3ZmVmNGE3ODQ1YzgzNDBjZDNjZWJmZTY3OTQxMmYzNTNhY2E3MzdiZjMiLCJpYXQiOjE2Njg0NDY3NjMuOTg4MTY2LCJuYmYiOjE2Njg0NDY3NjMuOTg4MTcxLCJleHAiOjE2Njg0ODI3NjMuOTc4OTQ3LCJzdWIiOiI0NiIsInNjb3BlcyI6W119.G_-w8NBxN9ztNDvZKW6V_V-DIXsBDjvQ5n0KjR91UShWVitrqi-QOZo8IhyzEBMzK5Vyvzyu6X36R5uAXs8juov_5uUQ7KS-ygT8RRcDthRJNvRt77jvKVrbD-GjjtJ13mqmatZbv31_cdWaScGa44JAWbE4QdDx03TGqtiO8bA';
    }

    public static function baseUrl(): string
    {
        return 'https://3dss-test.placetopay.com/api';
    }

    public static function DataSettings(): array
    {
        return [
            'token' => self::token(),
            'baseUrl' => self::baseUrl(),
        ];
    }
}
