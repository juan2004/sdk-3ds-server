<?php

namespace Tests\Unit\Requests;

use PHPUnit\Framework\TestCase;
use Placeto\Sdk3dsServer\Requests\MerchantRequest;
use Placeto\Sdk3dsServer\Server3DS;
use Tests\Unit\Config;

class MerchantRequestTest extends TestCase
{
    public function testCorrectlyAnalyzeTheCreationOfAMerchant(): void
    {
        $data = $this->DataMerchant();

        $request = new MerchantRequest($data);

        $this->assertEquals($data['name'], $request->getName());
        $this->assertEquals($data['brand'], $request->getBrand());
        $this->assertEquals($data['country'], $request->getCountry());
        $this->assertEquals($data['currency'], $request->getCurrency());
        $this->assertEquals($data['document'], $request->getDocument());
        $this->assertEquals($data['url'], $request->getUrl());
        $this->assertEquals($data['mcc'], $request->getMcc());
        $this->assertEquals($data['isicClass'], $request->getIsicClass());
        $this->assertEquals($data['branch'], $request->getBranch());
        $this->assertEquals($data['subscriptions'], $request->getSubscriptions());
        $this->assertEquals($data['invitations'], $request->getInvitations());

        $this->assertEquals($data, $request->toArray());
    }

    public function testCreateMerchant(): void
    {
        $response = (new Server3DS(Config::dataSettings()))->createMerchant($this->DataMerchant())->getResponse();

        $this->assertEquals(1000, $response['status']['code']);
        $this->assertArrayHasKey('data', $response);
    }

    public function testGetToken(): void
    {
        $response = (new Server3DS(Config::dataSettings()))->createMerchant($this->DataMerchant());

        $this->assertEquals($response->getResponse()['data']['token'], $response->getToken());
    }

    private function DataMerchant(): array
    {
        return [
            'name' => 'EGM Ingenieria sin froteras' . mt_rand(),
            'brand' => 'placetopay',
            'country' => 'COL',
            'currency' => 'COP',
            'document' => [
                'type' => 'RUT',
                'number' => '123456789-0',
            ],
            'url' => 'https://www.placetopay.com',
            'mcc' => 742,
            'isicClass' => 111,
            'branch' => [
                'name' => 'Ofic principal.' . mt_rand(),
                'brand' => 'placetopay dos',
                'country' => 'COL',
                'currency' => 'COP',
            ],
            'subscriptions' => [
                [
                    'franchise' => 1,
                    'acquirerBIN' => 400900,
                    'version' => 2,
                ],
            ],
            'invitations' => [
                'admin@admin.com' => 'juan.pabon@evertecinc.com',
            ],
        ];
    }
}
