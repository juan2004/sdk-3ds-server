<?php

namespace Tests\Unit\Requests;

use PHPUnit\Framework\TestCase;
use Placeto\Sdk3dsServer\Requests\BranchesMerchantsRequest;
use Placeto\Sdk3dsServer\Server3DS;
use Tests\Unit\Config;

class BranchesMerchantsRequestTest extends TestCase
{
    private function DataBranches(): array
    {
        return [
            'branches' => [
                [
                    'name' => 'namebranch ' . mt_rand(),
                    'brand' => 'Evertec Medellin',
                    'country' => 'COL',
                    'currency' => 'COP',
                    'url' => 'https://example-uno.com',
                ],
                [
                    'name' => 'namebranch2 ' . mt_rand(),
                    'brand' => 'Evertec Bogotá',
                    'country' => 'COL',
                    'currency' => 'COP',
                    'url' => 'https://example-dos.com',
                ],
            ],
        ];
    }

    public function testCorrectlyAnalyzeTheAddBranchesToAMerchant(): void
    {
        $data = $this->DataBranches();

        $request = new BranchesMerchantsRequest($data);

        $this->assertEquals($data['branches'], $request->getBranches());

        $this->assertEquals($data, $request->toArray());
    }

    public function testAddBranchesMerchants(): void
    {
        $merchantID = '135';

        $response = (new Server3DS(Config::dataSettings()))->addBranchesMerchants($this->DataBranches(), $merchantID)->getResponse();

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(1000, $response['status']['code']);
    }

    public function testTheBranchesFieldIsMandatory(): void
    {
        $merchantID = '135';

        $response = (new Server3DS(Config::dataSettings()))->addBranchesMerchants([], $merchantID)->getResponse();

        $this->assertArrayNotHasKey('data', $response);
        $this->assertEquals('The branches field is required.', $response['status']['error']);
        $this->assertEquals(1001, $response['status']['code']);
    }
}
