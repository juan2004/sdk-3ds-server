<?php

namespace Tests\Unit\Requests;

use PHPUnit\Framework\TestCase;
use Placeto\Sdk3dsServer\Requests\SessionRequest;
use Placeto\Sdk3dsServer\Server3DS;
use Tests\Unit\Config;

class SessionRequestTest extends TestCase
{
    private function DataSession(): array
    {
        return [
            'acctNumber' => '4111111111111111',
            'cardExpiryDate' => '2411',
            'purchaseAmount' => '8.25',
            'redirectURI' => 'http://www.placetopay.com',
            'purchaseCurrency' => 'COP',
        ];
    }

    public function testCorrectlyAnalyzeTheCreationOfASession(): void
    {
        $data = $this->DataSession();

        $request = new SessionRequest($data);

        $this->assertEquals($data['acctNumber'], $request->getAcctNumber());
        $this->assertEquals($data['cardExpiryDate'], $request->getCardExpiryDate());
        $this->assertEquals($data['purchaseAmount'], $request->getPurchaseAmount());
        $this->assertEquals($data['redirectURI'], $request->getRedirectURI());
        $this->assertEquals($data['purchaseCurrency'], $request->getPurchaseCurrency());

        $this->assertEquals($data, $request->toArray());
    }

    public function testCreateSession(): void
    {
        $response = (new Server3DS(Config::dataSettings()))->createSession($this->DataSession())->getResponse();

        $this->assertIsArray($response);
    }
}
