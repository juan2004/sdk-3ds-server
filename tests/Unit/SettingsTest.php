<?php

namespace Tests\Unit;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Placeto\Sdk3dsServer\Carrier\Request;
use Placeto\Sdk3dsServer\Exceptions\Server3DSException;
use Placeto\Sdk3dsServer\Helpers\Settings;
use Placeto\Sdk3dsServer\Server3DS;

class SettingsTest extends TestCase
{
    public function testNoTokenProvided(): void
    {
        $this->expectException(Server3DSException::class);
        $this->expectExceptionMessage('No token provided');

        $server3DS = new Server3DS([
            'baseUrl' => Config::baseUrl(),
        ]);
    }

    public function testNoBaseUrlProvided(): void
    {
        $this->expectException(Server3DSException::class);
        $this->expectExceptionMessage('No service URL provided to use');

        $server3DS = new Server3DS([
            'token' => Config::token(),
        ]);
    }

    public function testBaseUrlOptions(): void
    {
        $server3DS1 = new Server3DS(Config::DataSettings());

        $this->assertInstanceOf(Server3DS::class, $server3DS1);

        $server3DS2 = new Server3DS(Config::DataSettings());

        $this->assertInstanceOf(Server3DS::class, $server3DS2);
    }

    public function testCreateAClient(): void
    {
        $client = (new Settings(Config::DataSettings()))->getClient();

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testCreateRequest(): void
    {
        $request = (new Settings(Config::DataSettings()))->getServer3DSOperations();

        $this->assertInstanceOf(Request::class, $request);
    }

    public function testCreateUrl(): void
    {
        $baseUrl = Config::baseUrl();

        $url = (new Settings(Config::DataSettings()))->baseUrl('example');

        $this->assertEquals($baseUrl . '/example', $url);
    }

    public function testTransactionInformation(): void
    {
        $transactionID = '135';

        $response = (new Server3DS(Config::dataSettings()))->transactionInformation($transactionID)->getResponse();

        $this->assertIsArray($response);
    }
}
